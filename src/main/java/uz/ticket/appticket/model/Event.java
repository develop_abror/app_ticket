package uz.ticket.appticket.model;

import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Data
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description ;
    private Timestamp time;
    @ManyToOne
    private Hall hall;
    private Integer maxTicketAmount;
    private Double price;
}
