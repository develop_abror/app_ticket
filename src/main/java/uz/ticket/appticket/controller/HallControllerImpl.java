package uz.ticket.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.HallDTO;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.service.HallService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class HallControllerImpl implements HallController {
    final HallService hallService;


    public ResultMessage addHoll(HallDTO hall) {
        return hallService.addHoll(hall);
    }


    public List<Hall> getHallList() {
        return hallService.getHallList();
    }

    @Override
    public Hall getHall(Integer id) {
        return hallService.getHall(id);
    }

    @Override
    public ResultMessage editHall(Integer id, HallDTO hall) {
        return hallService.editHall(id,hall);
    }

    @Override
    public ResultMessage deleteHall(Integer id) {
        return hallService.deleteHall(id);
    }
}
