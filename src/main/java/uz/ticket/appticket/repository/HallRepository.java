package uz.ticket.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.appticket.model.Hall;

public interface HallRepository extends JpaRepository<Hall,Integer> {
}
