package uz.ticket.appticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultMessage {
    private Boolean success;
    private String message;
}
