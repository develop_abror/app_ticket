package uz.ticket.appticket.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.HallDTO;
import uz.ticket.appticket.payload.ResultMessage;

import java.util.List;

@RequestMapping("/api/holl")
public interface HallController {
    // C-R-U-D

    @PostMapping
    ResultMessage addHoll(@RequestBody HallDTO hallDTO);

    @GetMapping
    List<Hall> getHallList();

    @GetMapping("/{id}")
    Hall getHall(@PathVariable Integer id);

    @PutMapping("/{id}")
    ResultMessage editHall(@PathVariable Integer id,@RequestBody HallDTO hall);

    @DeleteMapping("/{id}")
    ResultMessage deleteHall(@PathVariable Integer id);
}
