package uz.ticket.appticket.payload;

import lombok.Data;

@Data
public class HallDTO {
    private String name;
    private String location;
    private Integer capacity;
}
