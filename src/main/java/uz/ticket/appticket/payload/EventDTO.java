package uz.ticket.appticket.payload;

import lombok.Data;

@Data
public class EventDTO {
    private String name;
    private String description;
    private String time;
    private Integer hallId;
    private Integer maxTicketAmount;
    private Double price;
}
