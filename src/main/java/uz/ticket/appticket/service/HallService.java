package uz.ticket.appticket.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.HallDTO;
import uz.ticket.appticket.payload.ResultMessage;

import java.util.List;


public interface HallService {
    ResultMessage addHoll(HallDTO hall);

    List<Hall> getHallList();

    Hall getHall( Integer id);

    ResultMessage editHall( Integer id, HallDTO hall);

    ResultMessage deleteHall( Integer id);
    // shuni tugatish kerak
//    void buYengiFunction();
}
