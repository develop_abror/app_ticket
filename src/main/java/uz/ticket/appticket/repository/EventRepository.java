package uz.ticket.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.appticket.model.Event;

public interface EventRepository extends JpaRepository<Event,Integer> {
}
