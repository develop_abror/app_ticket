package uz.ticket.appticket.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.EventDTO;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.repository.EventRepository;
import uz.ticket.appticket.repository.HallRepository;

import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class EventControllerImpl implements EventController{
    final HallRepository hallRepository;
    final EventRepository eventRepository;


    @Override
    public ResultMessage addEvent(EventDTO eventDTO) {
        Event event=new Event();
        event.setName(eventDTO.getName());

        Optional<Hall> optionalHall = hallRepository.findById(eventDTO.getHallId());
        if (optionalHall.isEmpty()) {
            return new ResultMessage(false,"");
        }
        Hall hall = optionalHall.get();
        event.setHall(hall);
        eventRepository.save(event);
        return null;
    }

    @Override
    public Event getEvent(Integer id) {
        Optional<Event> optionalEvent = eventRepository.findById(id);

        return null;
    }

}
