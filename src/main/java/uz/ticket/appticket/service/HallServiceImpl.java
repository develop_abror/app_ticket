package uz.ticket.appticket.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.HallDTO;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.repository.HallRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class HallServiceImpl implements HallService {
    private final HallRepository hallRepository;


    @Override
    public ResultMessage addHoll(HallDTO hallDTO) {
        Hall hall = hallMapper(new Hall(), hallDTO);
//        System.out.println(hall);
        hallRepository.save(hall);
        return new ResultMessage(true,"saved");
    }

    private Hall hallMapper(Hall hall, HallDTO hallDTO) {
        hall.setName(hallDTO.getName());
        hall.setLocation(hallDTO.getLocation());
        hall.setCapacity(hallDTO.getCapacity());
        return hall;
    }

    @Override
    public List<Hall> getHallList() {
        return hallRepository.findAll();
    }

    @Override
    public Hall getHall(Integer id) {
        Optional<Hall> optionalHall = hallRepository.findById(id);

        return optionalHall.orElseGet(null);
    }

    @SneakyThrows
    @Override
    public ResultMessage editHall(Integer id, HallDTO hallDTO) {
        Optional<Hall> optionalHall = hallRepository.findById(id);

        if (optionalHall.isEmpty()) {
            return new ResultMessage(false, "hall not found with id = "+id);
        }
        Hall hall = optionalHall.get();
        hallMapper(hall,hallDTO);
        hallRepository.save(hall);
        return new ResultMessage(true,"saved");
    }

    @Override
    public ResultMessage deleteHall(Integer id) {
        if (!hallRepository.existsById(id)) {
            return new ResultMessage(false, "hall not found with id = "+id);
        }
        hallRepository.deleteById(id);
        return new ResultMessage(true, "successfully deleted");
    }
}
